import {Phone} from "./core/phone/phone.service";
console.log('app.module.tsx')
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {UpgradeModule} from "@angular/upgrade/static";
import { HttpModule } from '@angular/http';
@NgModule({
    declarations: [],
    imports: [
        BrowserModule,
        UpgradeModule,
        HttpModule
    ],
    providers: [
        Phone
    ]
})

export class AppModule {
    ngDoBootstrap() {
        console.log('app.module.ts: bootstraped')
    }
}
