console.log('main.ts')
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
console.log('imported platformBrowserDynamic')
import { AppModule } from './app.module';
console.log('main.ts')
import {UpgradeModule} from "@angular/upgrade/static";
console.log('main.ts')
platformBrowserDynamic().bootstrapModule(AppModule).then( (platformRef) => {
    const upgrade = platformRef.injector.get(UpgradeModule) as UpgradeModule;
    upgrade.bootstrap(document.documentElement, ['phonecatApp']);
    console.log('main.ts: bootstraped in promise')
});
