# AngularJS Phone Catalog UPGRADE Tutorial 


## Overview

This application is from the finished version 
of the angular-phonecat-tutorial which was 
written in AngularJS. This README describes 
the steps of the 
[Upgrade-Tutorial from AngularJS to Angular2](https://angular.io/docs/ts/latest/guide/upgrade.html#phonecat-upgrade-tutorial).

### How to use
- Installing ``npm install``
- Run the application ``npm start``
- Run unit-tests 
  - First set environment for Chrome ``CHROME_BIN`` which is in case of chromium ``/usr/bin/chromium-browser`` normally 
  - Run ``npm run test`` for unit tests.
- Run end-to-end tests 
  - first start the application ``npm start``
  - and in another bash run ``npm run protractor``
  - **HINT:** In the case that port 3000 is already in use the application starts with another port, then your e2e-test won't run, cause it's configured to port 3000 in protractor.conf.js.

### Step1: Bootstrapping a hybrid Phonecat.

Includes the steps of TypeScript conversion 
and setup of unit-tests (karma) for AngularJS- and 
Angular2-Parts and also setup for e2e-tests.
