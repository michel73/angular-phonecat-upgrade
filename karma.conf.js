//jshint strict: false
module.exports = function(config) {
  config.set({

    basePath: '',

    files: [
      'app/bower_components/angular/angular.js',
      'app/bower_components/angular-animate/angular-animate.js',
      'app/bower_components/angular-resource/angular-resource.js',
      'app/bower_components/angular-route/angular-route.js',
      'app/bower_components/angular-mocks/angular-mocks.js',
      'app/core/phone/phone.module.js',
//       'app/core/phone/phone.service.js',
      'app/core/core.module.js',
      'app/phone-detail/phone-detail.module.js',
      'app/phone-detail/phone-detail.component.js',
      'app/phone-list/phone-list.module.js',
      'app/phone-list/phone-list.component.js',
      'app/core/checkmark/checkmark.filter.js',
      'app/core/checkmark/checkmark.filter.js',
//       'app/app.module.ng1.js',
//       'app/main.js',
      'app/**/*.module.js',
      'app/*!(.module|.spec).js',
      'app/!(bower_components)/**/*!(.module|.spec).js',
      'app/**/*.spec.ng2.js',
//       'node_modules/@angular/upgrade/bundles/upgrade.umd.js',

      'node_modules/systemjs/dist/system.src.js',
// Polyfills
      'node_modules/core-js/client/shim.js',
// zone.js
      'node_modules/zone.js/dist/zone.js',
      'node_modules/zone.js/dist/long-stack-trace-zone.js',
      'node_modules/zone.js/dist/proxy.js',
      'node_modules/zone.js/dist/sync-test.js',
      'node_modules/zone.js/dist/jasmine-patch.js',
      'node_modules/zone.js/dist/async-test.js',
      'node_modules/zone.js/dist/fake-async-test.js',

      {pattern: 'karma-test-shim.js', included: true, watched: true},
// RxJs.
      { pattern: 'node_modules/rxjs/**/*.js', included: false, watched: false },
      { pattern: 'node_modules/rxjs/**/*.js.map', included: false, watched: false },
// Angular itself and the testing library
      {pattern: 'node_modules/@angular/**/*.js', included: false, watched: true},
      {pattern: 'node_modules/@angular/**/*.js.map', included: false, watched: true},

//       {pattern: 'systemjs.config.js', included: false, watched: false},
      {pattern: 'app/**/*.module.js', included: true, watched: true},
      {pattern: 'app/*!(.module|.spec).js', included: false, watched: true},
      {pattern: 'app/!(bower_components)/**/*!(.module|.spec).js', included: false, watched: true},
      {pattern: 'app/**/*.spec.js', included: false, watched: true},
      {pattern: 'app/phone-detail/*.html', included: false, watched: true},
      {pattern: 'app/phone-list/*.html', included: false, watched: true},

    ],
//     exclude: [
//       'app/**/*.spec.js'
//     ],

    autoWatch: true,

    frameworks: ['jasmine'],

//     browsers: ['Chrome', 'Firefox'],
    browsers: ['Chrome'],
    customLaunchers: {
      Chrome: {
        base: 'Chromium',
        flags: ['--disable-gpu']
      }
    },


    // proxied base paths for loading assets
    proxies: {
      // required for component assets fetched by Angular's compiler
      "/phone-detail": '/base/app/phone-detail',
      "/phone-list": '/base/app/phone-list'
    },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    singleRun: false

  });
};
